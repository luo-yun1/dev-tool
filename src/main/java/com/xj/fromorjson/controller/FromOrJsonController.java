package com.xj.fromorjson.controller;

import com.alibaba.fastjson.JSONObject;
import com.xj.fromorjson.service.FromOrJsonService;
import com.xj.fromorjson.vo.ResEnv;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * From表单数据转换JSON
 *
 * @author LuoY
 * @date 2024/05/10 17:06
 **/
@Slf4j
@RestController
@RequestMapping("/from")
public class FromOrJsonController {

    @Resource
    private FromOrJsonService fromOrJsonService;

    @ApiOperation(value = "from转json")
    @GetMapping("/json")
    public ResEnv<JSONObject> json(String fromView) {
        JSONObject str = fromOrJsonService.fromToJson(fromView);
        return  ResEnv.success(str);
    }
}
