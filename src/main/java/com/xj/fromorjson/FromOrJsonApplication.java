package com.xj.fromorjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FromOrJsonApplication {
    public static void main(String[] args) {
        SpringApplication.run(FromOrJsonApplication.class, args);
    }

}
