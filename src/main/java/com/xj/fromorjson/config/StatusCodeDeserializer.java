package com.xj.fromorjson.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.xj.fromorjson.enums.StatusCode;


import java.io.IOException;
import java.util.HashMap;

public class StatusCodeDeserializer extends JsonDeserializer<StatusCode> implements ContextualDeserializer {
    private HashMap<String, Enum<?>> nameMap;

    @Override
    public StatusCode deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        String name = readNameValue(jp);
        return (StatusCode) nameMap.get(name);
    }


    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt,
                                                BeanProperty property) throws JsonMappingException {
        JavaType type = property.getType();
        Class clazz = type.getRawClass();

        Enum<?>[] enumValues = ((Class<Enum<?>>) clazz).getEnumConstants();
        if (enumValues == null) {
            throw new IllegalArgumentException("No enum constants for class " + clazz.getName());
        }

        HashMap<String, Enum<?>> map = new HashMap<>();
        for (Enum<?> enumValue : enumValues) {
            map.put(enumValue.name(), enumValue);
        }

        StatusCodeDeserializer deserializer = new StatusCodeDeserializer();
        deserializer.setNameMap(map);
        return deserializer;
    }

    private String readNameValue(JsonParser jp) throws IOException {
        JsonToken token = jp.currentToken();
        String name = null;
        if (token == JsonToken.START_OBJECT) {
            while (!jp.isClosed() && token != JsonToken.END_OBJECT) {
                token = jp.nextToken();
                if (token == JsonToken.FIELD_NAME
                        && "status".equals(jp.getCurrentName())) {
                    jp.nextToken();
                    name = jp.getValueAsString();
                }
            }
        } else {
            name = jp.getValueAsString();
        }
        return name;
    }

    public HashMap<String, Enum<?>> getNameMap() {
        return nameMap;
    }

    private void setNameMap(HashMap<String, Enum<?>> nameMap) {
        this.nameMap = nameMap;
    }
}