package com.xj.fromorjson.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.xj.fromorjson.enums.StatusCode;


import java.io.IOException;

public class StatusCodeSerializer extends StdSerializer<StatusCode> {
    public StatusCodeSerializer() {
        super(StatusCode.class);
    }

    @Override
    public void serialize(StatusCode value, JsonGenerator generator, SerializerProvider provider)
            throws IOException {
        generator.writeStartObject();
        generator.writeFieldName("status");
        generator.writeNumber(value.getStatus());
        generator.writeFieldName("message");
        generator.writeString(value.getMessage());
        generator.writeEndObject();
    }
}