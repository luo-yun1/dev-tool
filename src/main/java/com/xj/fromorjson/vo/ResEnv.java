package com.xj.fromorjson.vo;

import com.xj.fromorjson.enums.SysStatusCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 接口统一返回类
 *
 * @author LuoY
 * @date 2024/05/10 17:12
 **/
@ApiModel("接口统一返回实体")
public class ResEnv<T> implements Serializable {
    private String uid;

    @ApiModelProperty("状态码")
    private int status;

    @ApiModelProperty("返回消息")
    private String message;

    @ApiModelProperty("具体数据对象")
    private T data;
    @ApiModelProperty("总记录数")
    private Long total;
    @ApiModelProperty("页码")
    private Integer pageNum;
    @ApiModelProperty("分页大小")
    private Integer pageSize;
    @ApiModelProperty("总分页数")
    private Integer pages;
    @ApiModelProperty("扩展数据对象")
    private Map<String, Object> options;

    public static <T> ResEnv<T> success(T object) {
        return new ResEnv<>(SysStatusCode.SUCCESS.getStatus(), SysStatusCode.SUCCESS.getMessage(), object);
    }

    public static <T> ResEnv<T> success() {
        return success(SysStatusCode.SUCCESS.getMessage());
    }

    public static <T> ResEnv<T> success(String msg) {
        return new ResEnv<>(SysStatusCode.SUCCESS.getStatus(), msg, null);
    }

    public static <T> ResEnv<T> success(String msg, T object) {
        return new ResEnv<>(SysStatusCode.SUCCESS.getStatus(), msg, object);
    }

    public static <T> ResEnv<T> fail(String msg, Integer status) {
        return new ResEnv<>(status, msg, null);
    }

    public static <T> ResEnv<T> fail(String msg) {
        return fail(msg, SysStatusCode.SERVER_ERROR.getStatus());
    }

    public static <T> ResEnv<T> fail() {
        return fail(SysStatusCode.SERVER_ERROR.getMessage());
    }

    public ResEnv() {
    }

    public ResEnv(Integer status) {
        this();
        this.status = status;
    }

    public ResEnv(Integer status, String message) {
        this(status);
        this.message = message;
    }

    public ResEnv(Integer status, String message, T data) {
        this(status, message);
        this.data = data;
    }

    public ResEnv addOption(String key, Object value) {
        if (options == null) {
            options = new HashMap<>();
        }
        options.put(key, value);
        return this;
    }

    public <T> T getOption(String key) {
        return options == null ? null : (T) options.get(key);
    }

    public boolean hasOption(String key) {
        return options == null ? false : options.containsKey(key);
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public Map<String, Object> getOptions() {
        return options;
    }

}

