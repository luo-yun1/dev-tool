package com.xj.fromorjson.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author LuoY
 * @date 2024/05/10 17:08
 **/
public interface FromOrJsonService {
    /**
     * 功能描述：from表单数据转换json
     *
     * @param： fromView
     * @return {@link String }
     **/
    JSONObject fromToJson(String fromView);
}
