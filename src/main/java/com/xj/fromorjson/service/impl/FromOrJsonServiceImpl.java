package com.xj.fromorjson.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.xj.fromorjson.service.FromOrJsonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

/**
 * @author LuoY
 * @date 2024/05/10 17:08
 **/
@Service
@Slf4j
public class FromOrJsonServiceImpl implements FromOrJsonService {

    /**
     * 将指定的字符串转换为JSON对象。
     * 字符串格式应为键值对以"&"分隔，键值对之间以"="分隔。如果值不存在，则默认为0。
     *
     * @param fromView 需要转换的字符串。
     * @return 转换后的JSON对象。
     */
    @Override
    public JSONObject fromToJson(String fromView) {
        // 以"&"分割输入字符串为键值对数组
        String[] stars = fromView.split("&");
        JSONObject obj = new JSONObject();
        for (String each : stars) {
            // 以"="分割键值对
            String[] params = each.split("=");
            if (params.length == 1) {
                // 如果只有键没有值，则将值设为0
                obj.put(params[0], 0);
            } else if (params.length == 2) {
                // 如果有键和值，则将值解码后放入JSON对象
                obj.put(params[0], URLDecoderString(params[1]));
            }
        }
        return obj;
    }

    /**
     * 解码URL编码的字符串。
     *
     * @param str 需要解码的URL编码字符串。
     * @return 如果输入字符串为null，返回空字符串；如果解码成功，返回解码后的字符串；如果解码过程中发生异常，返回空字符串。
     */
    private static String URLDecoderString(String str) {
        // 如果输入字符串为null，直接返回空字符串
        if (str == null) {
            return "";
        }
        try {
            // 尝试使用UTF-8编码解码输入字符串
            return java.net.URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // 如果遇到不支持的编码异常，打印堆栈跟踪信息
            e.printStackTrace();
        }
        // 如果发生异常或解码失败，返回空字符串
        return "";
    }
}
