package com.xj.fromorjson.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xj.fromorjson.config.StatusCodeDeserializer;
import com.xj.fromorjson.config.StatusCodeSerializer;

/**
 * @author LuoY
 * @date 2024/05/10 17:16
 **/
@JsonSerialize(using = StatusCodeSerializer.class)
@JsonDeserialize(using = StatusCodeDeserializer.class)
public interface StatusCode {
    int getStatus();
    String getMessage();
}
