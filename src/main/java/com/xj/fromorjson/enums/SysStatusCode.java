package com.xj.fromorjson.enums;

/**
 * 返回统一类的通义状态码
 *
 * @author LuoY
 * @date 2024/05/10 17:15
 **/
public enum SysStatusCode implements StatusCode{
    SUCCESS(200, "操作成功"),
    INVALID_PARAM(400, "参数异常"),
    UNAUTHORIZED(401,"请求未授权"),
    FORBIDDEN(403, "拒绝访问"),
    NOT_FOUND(404, "服务器找不到请求的资源"),
    METHOD_NOT_ALLOWED(405, "请求方法(Method)不支持"),
    REQUEST_TIMEOUT(408, "请求超时"),
    GONE(410, "资源不存在"),
    UNSUPPORTED_MEDIA_TYPE(415, "不被支持的媒体类型(content-type)"),
    LICENSE_EXPIRE(444, "许可证书已过期"),
    SERVER_ERROR(500,"服务异常"),
    SERVICE_UNAVAILABLE(503,"服务不可用"),

    DS_OPT_ERROR(8000, "操作数据库出错"),
    DS_CONNECTION_ERROR(8001,"数据库连接失败");

    private final int status;
    private final String message;

    SysStatusCode(int status, String message) {
        this.status = status;
        this.message = message;
    }


    public static StatusCode valueOf(int status) {
        for (SysStatusCode statusCode : values()) {
            if (statusCode.status == status) {
                return statusCode;
            }
        }
        throw new IllegalArgumentException("No matching constant for [" + status + "]");
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
